export {
    cd, pwd, local, ssh, mv, cp, rm
};

const url = 'http://localhost:42990/';
//const url = 'http://ketabuk.org:42990/';
//const url = 'http://dank-machine:42990/';

function cd(ctx, user_data, path, onsuccess, onfailure)
{
    if(path == null)
    {
        throw "the path passed to cd() is null";
    }

    if(path.length != 0 && path[path.length-1] != '/')
    {
        path += '/';
    }

    if(path != "")
    {
        if(path[0] == '/')
        {
            ctx.directory = path;
        }
        else
        {
            ctx.directory += path;
        }
    }
    else
    {
        ctx.directory = path;
    }

    
    http_post(url + 'cd', JSON.stringify(ctx),
        (response) => {
            //log_success(response);
            response = JSON.parse(response);
            onsuccess(user_data, response);
        },
        (status, response) => {
            log_failure(status, response);
            onfailure(status, response);
        });   
}

function pwd(ctx, user_data, onsuccess)
{
    onsuccess(user_data, "/");
}

function local(user_data, onsuccess, onfailure)
{
    var ctx = {
        connection_id: 0,
        directory: "/"
    };
    onsuccess(ctx, user_data);
}

function ssh(user_data, user_at_host, onsuccess, onfailure)
{
    http_post('/connect', user_at_host,
        (response) => {
            //log_success(response);
            onsuccess(response, user_data);
        },
        (status, response) => {
            log_failure(status, response);
            onfailure(status, response);
        });    
}

function cp(ctx, user_data, sources, destination, onsuccess, onfailure)
{
    const request_body = {
        connection_id: ctx.connection_id,
        sources: sources,
        destination: destination
    };

    http_post(url + 'cp', JSON.stringify(request_body),
        (response) => {
            //log_success(response);
            response = JSON.parse(response);
            onsuccess(user_data, response);
        },
        (status, response) => {
            log_failure(status, response);
            onfailure(status, response);
        });
}

function rm(ctx, user_data, path, onsuccess, onfailure)
{
    onsuccess(user_data);
}

function mv(ctx, user_data, source, destination, onsuccess, onfailure)
{
    onsuccess(user_data);
}

function http_post(url, data, onsuccess, onfailure)
{
    http_request(url, "POST", data, onsuccess, onfailure);
}

function http_request(url, method, data, onsuccess, onfailure)
{
    //console.log("sending request");
    var http = new XMLHttpRequest();
    http.open(method, url, true); // true for asynchronous
    http.setRequestHeader('Content-Type', 'application/json');
    http.send(data);

    http.onreadystatechange = function()
    {
        if (http.readyState == 4 && http.status == 200)
        {
            onsuccess(http.responseText);
            return;
        }
        else if(http.readyState == 4 && http.status != 200)
        {
            onfailure(http.status, http.responseText);
        }
    }
}

function log_success(response)
{
    console.log("response received");
    console.log("response: " + response);
    console.log("Server responded with 200 success");
}

function log_failure(status, response)
{
    console.log("Request failed, server responded with: ");
    console.log(response);
    console.log("response code: " + status);
}