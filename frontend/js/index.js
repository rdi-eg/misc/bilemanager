import {
    init          as ui_init,
    ls            as ui_ls,
    display_error as ui_display_error
}
from './ui.js';

import { local, cd, cp } from './service.js';

function _on_cd_request(ctx, path)
{
    ctx.requested_cd = path;
    cd(ctx.connection, ctx, path, _on_cd_success, _on_cd_failure);
}

function _on_split_request(ctx)
{
    var new_ctx = Object.assign({}, ctx);

    new_ctx.window = ui_init(
        new_ctx,
        new_ctx.current_directory,
        _on_cd_request,
        _on_operation_request,
        _on_split_request,
    );

    local(new_ctx, _on_connection_success, _on_connection_failure);
}

function _on_operation_request(ctx, clipboard)
{
    if(clipboard.operation == "Cut")
    {

    }
    if(clipboard.operation == "Copy")
    {
        cp(ctx.connection, ctx, clipboard.sources, clipboard.destination,
           _on_operation_success, _on_operation_failure);
    }
    if(clipboard.operation == "Paste")
    {
        
    }
    if(clipboard.operation == "Rename")
    {
        
    }
    if(clipboard.operation == "Delete")
    {
        
    }
}

function _on_cd_failure(ctx, error)
{
    ui_display_error(ctx.window, error);
    ctx.requested_cd = null;
}

function _on_cd_success(ctx, listing)
{
    ctx.current_directory = ctx.requested_cd;
    ctx.requested_cd = null;
    ui_ls(ctx.window, ctx.current_directory, listing);
}

function _on_operation_failure(ctx, error)
{
    ui_display_error(ctx.window, error);
}

function _on_operation_success(ctx, listing)
{
    ui_ls(ctx.window, ctx.current_directory, listing);
}

function _on_connection_success(connection, ctx)
{
    ctx.connection = connection;
    ctx.current_directory = connection.directory;
    ctx.requested_cd = connection.directory;
    cd(ctx.connection, ctx, connection.directory, _on_cd_success, _on_cd_failure);
}

function _on_connection_failure(ctx, error)
{
    ui_display_error(ctx.window, error);
}

var ctx1 = {
    connection: null,
    window: null,
    current_directory: "/",
    requested_cd: null,
};

ctx1.window = ui_init(
    ctx1,
    ctx1.current_directory,
    _on_cd_request,
    _on_operation_request,
    _on_split_request,
);

local(ctx1, _on_connection_success, _on_connection_failure);