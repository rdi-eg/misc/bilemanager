package main

import (
    "os"
    "io"
    "fmt"
    "log"
    "bufio"
    "context"
    "strings"
    "os/user"
    "os/exec"
    "net/http"
    "io/ioutil"
    "encoding/json"
)

func main() {
    port := "42990"
    http.Handle("/", http.FileServer(http.Dir("./frontend")))

    http.HandleFunc("/connect", connect)
    http.HandleFunc("/cd", cd)
    http.HandleFunc("/mv", mv)
    http.HandleFunc("/cp", cp)
    http.HandleFunc("/rm", rm)

    var c connection

    currentUser, _ := user.Current();

    c.user = currentUser.Username

    c.host = "localhost"

    home, err := os.UserHomeDir(); if err != nil {
        log.Fatal(err)
        os.Exit(1)
    }

    c.workingDir = home
    connections = append(connections, c)

    fmt.Println("Server started at port", port)
    log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}

type processPipes struct {
    stdin io.WriteCloser
    stdout io.ReadCloser
    stderr io.ReadCloser
}

type connection struct {
    user string
    host string
    workingDir string
    pipes processPipes
    cancel context.CancelFunc
}

var connections []connection

func connect(w http.ResponseWriter, r *http.Request) {
    body_, err := ioutil.ReadAll(r.Body)

    if err != nil {
        log.Printf("Error reading body: %v", err)
        http.Error(w, "can't read body", http.StatusBadRequest)
        return
    }

    body := string(body_)

    if len(body) == 0 {
        log.Printf("Error reading connection string '%s'", body)
        http.Error(w, "Error reading connection string", http.StatusBadRequest)
        return
    }

    var c connection
    v := strings.Split(body, ":")
    
    if len(v) > 2 {
        log.Printf("Error reading connection string '%s'", body)
        http.Error(w, "Error reading connection string", http.StatusBadRequest)
        return
    }

    if len(v) == 1 {
        c.workingDir = "~/"
    } else if len(v) == 2 {
        c.workingDir = v[1]
    }

    v = strings.Split(v[0], "@")


    if len(v) != 2 {
        log.Printf("Error reading connection string '%s'", body)
        http.Error(w, "Error reading connection string", http.StatusBadRequest)
        return
    }

    c.user = v[0]
    c.host = v[1]

    ctx, cancel := context.WithCancel(context.Background())
    cmd := exec.CommandContext(ctx, "ssh", fmt.Sprintf("%s@%s", c.user, c.host))

    c.pipes.stdin, err = cmd.StdinPipe(); if err != nil {
		log.Printf("Couldn't get stdin for ssh: '%s'", err)
        http.Error(w, "Could not connect", http.StatusInternalServerError)
        return
	}

    c.pipes.stdout, err = cmd.StdoutPipe(); if err != nil {
		log.Printf("Couldn't get stdout for ssh: '%s'", err)
        http.Error(w, "Could not connect", http.StatusInternalServerError)
        return
	}


    c.pipes.stderr, err = cmd.StderrPipe(); if err != nil {
		log.Printf("Couldn't get stderr for ssh: '%s'", err)
        http.Error(w, "Could not connect", http.StatusInternalServerError)
        return
	}

    c.cancel = cancel

    if err := cmd.Start(); err != nil {
		log.Printf("Could not connect: '%s'", err)
        http.Error(w, "Could not connect", http.StatusInternalServerError)
        return
	}

    connections = append(connections, c)
    //connectionId := len(connections) - 1

    ls(c)

    prompt := "TPuSnmnbrJKbSz4aSuWXoi7y45"

    io.WriteString(c.pipes.stdin, fmt.Sprintf("export PS1=%s\n", prompt))

    stdout := bufio.NewReader(c.pipes.stdout)
    
    for {
        sshOutput, err := stdout.ReadString('\n'); if err != nil {
            log.Printf("Could not read from ssh: %s", err)
            http.Error(w, "Could not ls", http.StatusInternalServerError)
            return
        }
        
        fmt.Fprintf(w, sshOutput)

        if strings.HasSuffix(sshOutput, prompt) {
            log.Printf("found prompt!")
            break
        }
    }

    log.Printf("handled request I guess")
}

type file struct {
    Name string `json:"name"`
    Type string `json:"type"`
}

type cdRequest struct {
    ConnectionId int `json:"connection_id"`
    Directory string `json:"directory"`
}

func ls(c connection) {
    
}

func cd(w http.ResponseWriter, r *http.Request) {
    setupResponse(&w)
	if (*r).Method == "OPTIONS" {
		return
	}

    var requestJson cdRequest
    if err := json.NewDecoder(r.Body).Decode(&requestJson); err != nil {
        log.Printf("Error reading body: %v", err)
        http.Error(w, "can't read body", http.StatusBadRequest)
        return
    }

    connectionId := requestJson.ConnectionId

    if connectionId >= len(connections) {
        log.Printf("Bad connectionId: %d", connectionId)
        http.Error(w, "Bad connectionId", http.StatusBadRequest)
        return
    }

    c := connections[connectionId]

    files, err := ioutil.ReadDir(requestJson.Directory); if err != nil {
        log.Printf("Error listing %s: %v", requestJson.Directory, err)
        http.Error(w, "Error listing", http.StatusBadRequest)
        return
    }

    c.workingDir = requestJson.Directory

    listing := make([]file, len(files))

    for i, f := range files {
        listing[i].Name = f.Name()
        listing[i].Type = "file"
        if f.IsDir() {
            listing[i].Type = "dir"
        }
    }

    err = json.NewEncoder(w).Encode(listing); if err != nil {
        log.Printf("Can't write json: %v", err)
        http.Error(w, "Can't write json", http.StatusInternalServerError)
        return
    }
}

func mv(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "mv\n")
}

type cpRequest struct {
    ConnectionId int   `json:"connection_id"`
    Sources []string   `json:"sources"`
    Destination string `json:"destination"`
}

func cp(w http.ResponseWriter, r *http.Request) {
    setupResponse(&w)
	if (*r).Method == "OPTIONS" {
		return
	}

    var requestJson cpRequest
    if err := json.NewDecoder(r.Body).Decode(&requestJson); err != nil {
        log.Printf("Error reading body: %v", err)
        http.Error(w, "can't read body", http.StatusBadRequest)
        return
    }

    connectionId := requestJson.ConnectionId

    if connectionId >= len(connections) {
        log.Printf("Bad connectionId: %d", connectionId)
        http.Error(w, "Bad connectionId", http.StatusBadRequest)
        return
    }

    c := connections[connectionId]

    if len(requestJson.Sources) < 1 {
        log.Printf("No sources in request body")
        http.Error(w, "No sources in request body", http.StatusBadRequest)
        return
    }

    for _, f := range requestJson.Sources {
        tokens := strings.Split(f, "/")

        filename := tokens[len(tokens) - 1]
        dst := fmt.Sprintf("%s%s", requestJson.Destination, filename)

        _, err := copy(f, dst); if err != nil {
            log.Printf("Couldn't copy %s to %s: %s", f, dst, err)
            http.Error(w, fmt.Sprintf("Couldn't copy %s to %s", f, dst), http.StatusBadRequest)
            return
        }
    }

    files, err := ioutil.ReadDir(c.workingDir); if err != nil {
        log.Printf("Error listing %s: %v", c.workingDir, err)
        http.Error(w, "Error listing", http.StatusBadRequest)
        return
    }

    listing := make([]file, len(files))

    for i, f := range files {
        listing[i].Name = f.Name()
        listing[i].Type = "file"
        if f.IsDir() {
            listing[i].Type = "dir"
        }
    }

    err = json.NewEncoder(w).Encode(listing); if err != nil {
        log.Printf("Can't write json: %v", err)
        http.Error(w, "Can't write json", http.StatusInternalServerError)
        return
    }
}

func copy(src, dst string) (int64, error) {
    sourceFileStat, err := os.Stat(src)
    if err != nil {
        return 0, err
    }

    if !sourceFileStat.Mode().IsRegular() {
        return 0, fmt.Errorf("%s is not a regular file", src)
    }

    source, err := os.Open(src)
    if err != nil {
        return 0, err
    }
    defer source.Close()

    destination, err := os.Create(dst)
    if err != nil {
        return 0, err
    }
    defer destination.Close()
    nBytes, err := io.Copy(destination, source)

    return nBytes, err
}

func rm(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "rm\n")
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func setupResponse(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
    (*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
    (*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}